#!/usr/bin/env node --harmony
var co = require('co');
var prompt = require('co-prompt');
var program = require('commander');
var config = require('./config.json');
var Genesis = require('./genesis.js');

function start() {
  let genesis = Genesis(config);
  genesis.auth(config.username, config.password)
    .then(resp => {
      if (program.backup) {
        return genesis.backup();
      }

      if (program.id && program.molecule) {
        return genesis.downloadAllMolecules(program.id);
      }

      if (program.id) {
        return genesis.downloadMoleculoid(program.id);
      }

      return genesis.downloadAllMoleculoids();
    })
    .then(resp => {
      console.log("\n\n👊  Feito!");
    })
    .catch(err => {
      console.log('\n\n😓  OPS.. \n', err);
    })
}

program
  .version('1.0.0')
  .option('-i, --id <id>', 'Moleculoid ID')
  .option('-m, --molecule', 'Download all molecules from moleculoid')
  .option('-b, --backup', 'Download all data')
  .action(start())
  .parse(process.argv);
