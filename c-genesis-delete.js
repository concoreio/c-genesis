#!/usr/bin/env node --harmony
var co = require('co');
var prompt = require('co-prompt');
var program = require('commander');
var config = require('./config.json');
var Genesis = require('./genesis.js');
var fs = require('fs');
var Concore = require('concore-sdk-js/lib/node/');

var deleteMoleculoid = function(name) {
  let genesis = Genesis(config);

  genesis.auth(config.username, config.password)
    .then(resp => {
      if (program.molecules) {
        console.log("🕐  Excluindo todas moléculas de " + name);
        return genesis.destroyAllMolecules(name);
      }

      console.log("🕐  Excluindo moleculoid...");
      return genesis.deleteMoleculoid(name);
    })
    .then(resp => {
      console.log(resp);
      console.log("👊  Feito!");
    })
    .catch(err => {
      if (err.response) {
        console.log('😓  OPS.. \n\n', err.response.data);
        return;
      }

      if (err.data) {
        console.log('😓  OPS.. \n\n', err.data);
        return;
      }

      console.log('😓  OPS.. \n\n', err);
    })
}

program
  .version('1.0.0')
  .arguments('<Moleculoid name>')
  .option('-m, --molecules', 'Set to destroy the molecules')
  .action(deleteMoleculoid)
  .parse(process.argv);
