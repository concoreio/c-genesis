#!/usr/bin/env node --harmony
var Concore = require('concore-sdk-js/lib/node/');
var co = require('co');
var prompt = require('co-prompt');
var program = require('commander');
var fs = require('fs');
var Genesis = require('./dist/genesis.js');
var config = require('./config.json');

var Genesis = function (config) {
  Concore.init(config.host, config.public, config.private);
  this.MetatomType = Concore.Datacore.MetatomType;
  this.Moleculoid = Concore.Datacore.Moleculoid;

  this.auth = function auth(user, password) {
    return Concore.Datacore.Auth.login(user, password);
  }

  this.createMoleculoid = function (jsonSchema) {
    const metatoms = [];

    for (var i = 0; i < jsonSchema.metatoms.length; i++) {
      let metatomSchema = jsonSchema.metatoms[i];
      metatomSchema.settings.order = i + 1;
      let metatom = null;

      if (metatomSchema.type === 'Choice') {
        metatom = new this.MetatomType[metatomSchema.type](metatomSchema.name, metatomSchema.options, metatomSchema.settings);
      } else if (metatomSchema.type === 'Reference') {
        metatom = new this.MetatomType.Reference(metatomSchema.name, metatomSchema.moleculoid, metatomSchema.settings);
      } else {
        metatom = new this.MetatomType[metatomSchema.type](metatomSchema.name, metatomSchema.settings);
      }

      metatoms.push(metatom);
    }

    const moleculoid = new this.Moleculoid(jsonSchema.moleculoidId, metatoms, {
      label: jsonSchema.label,
      icon: jsonSchema.icon
    });

    return moleculoid.save();
  }

  this.updateMoleculoid = function (schema) {
    this.auth()
      .then(() => {
        return this.Moleculoid.get(schema.moleculoidId);
      })
      .then(moleculoid => {
        const metatoms = moleculoid.getMetatoms();
        for (let metatom of metatoms) {
          console.log(`remove ${metatom.getName()}`);
          moleculoid.removeMetatoms(metatom);
        }

        for (let metatom of schema.metatoms) {
          console.log(`add ${metatom.getName()}`);
          moleculoid.addMetatoms(metatom);
        }

        return moleculoid.save();
      })
      .then(resp => {
        console.log(resp);
      })
      .catch(err => {
        console.log(err);
      });
  }

  this.delete = function (moleculoidName) {
    this.auth()
      .then(() => {
        return this.Moleculoid.get(moleculoidName)
      })
      .then(moleculoid => {
        return moleculoid.destroy();
      })
      .then(resp => {
        console.log(resp);
      })
      .catch(err => {
        console.log(err);
      });
  }

  return this;
}

var loadJsonFile = function(path) {
  var fileAsString = fs.readFileSync(path).toString();
  var jsonFile = JSON.parse(fileAsString);
  return jsonFile;
}

program
  .version('1.0.0')
  .arguments('<file>')
  .option('-c, --create', 'Create new Moleculoid from json')
  .action(function(schemaUrl) {
    let genesis = Genesis(config);

    genesis.auth(config.username, config.password)
      .then(resp => {
        if (program.create) {
          var jsonFile = loadJsonFile(schemaUrl);

          if (!jsonFile) {
            throw 'Erro ao tentar converter o json ou o arquivo esta vazio.';
            return;
          }

          return genesis.createMoleculoid(jsonFile);
        }
      })
      .then(resp => {
        console.log(resp);
      })
      .catch(err => {
        console.log(err);
      })
  })
  .parse(process.argv);