#!/usr/bin/env node --harmony
var co = require('co');
var prompt = require('co-prompt');
var program = require('commander');

program
  .version('1.0.0')
  .command('create', 'Create new Moleculoid from json')
  .command('update', 'Update a Moleculoid from json')
  .command('delete', 'Delete Moleculoid or Molecules')
  .command('download', 'Download all moleculoids and save as json')
  .command('upload', 'Upload backup files')
  .parse(process.argv);