#!/usr/bin/env node --harmony
var co = require('co');
var prompt = require('co-prompt');
var program = require('commander');
var config = require('./config.json');
var Genesis = require('./genesis.js');
var fs = require('fs');
var Concore = require('concore-sdk-js/lib/node/');

var loadJsonFile = function(path) {
  var fileAsString = fs.readFileSync(path).toString();
  var jsonFile = JSON.parse(fileAsString);
  return jsonFile;
}

var upload = function(dataJsonUrl) {
  let genesis = Genesis(config);

  console.log("🕐  Autenticando usuário: " + config.username);
  genesis.auth(config.username, config.password)
    .then(resp => {
      console.log("👍  Usuário autenticado");

      console.log("🕐  Carregando schema...");
      var jsonFile = loadJsonFile(dataJsonUrl);
      console.log("👍  Dados carregados!");

      if (!jsonFile) {
        throw '😓  Erro ao tentar converter o json ou o arquivo esta vazio.';
        return;
      }

      console.log("🕐  Salvando moleculas...");
      return genesis.uploadBackup(jsonFile);
    })
    .then(resp => {
      console.log("👊  Feito!");
    })
    .catch(err => {
      console.log('😓  OPS.. \n\n', err);
    })
}

program
  .version('1.0.0')
  .arguments('<file>')
  .action(upload)
  .parse(process.argv);
