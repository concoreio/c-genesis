var Concore = require('concore-sdk-js/node');
var fs = require('fs');
var Promise = require('promise');

var Genesis = function (config) {
  console.log("🛠  Concore configurado!\n📡  " + config.host + "\n🔑  " + config.public);
  Concore.init(config.host, config.public, config.private);
  this.MetatomType = Concore.Datacore.MetatomType;
  this.Moleculoid = Concore.Datacore.Moleculoid;

  this.auth = function auth(user, password) {
    console.log("👤  Usuário: " + config.username + "\n\n");
    return Concore.Datacore.Auth.login(user, password);
  }

  this._orderMetatoms = function (jsonSchema) {
    for (var i = 0; i < jsonSchema.metatoms.length; i++) {
      jsonSchema.metatoms[i].order = i;
    }

    return jsonSchema;
  }

  this.createMoleculoid = function (jsonSchema) {
    jsonSchema = this._orderMetatoms(jsonSchema);
    if (jsonSchema.group) {
      delete jsonSchema.group;
    }

    const moleculoid = this.Moleculoid._inflateMoleculoid(jsonSchema);
    if (!moleculoid) {
      console.log('Erro ao inflar moleculoid, verifique o schema json');
    }

    return moleculoid.save();
  }

  this.uploadBackup = function (jsonSchema) {
    var molecules = [];

    for (let moleculeJson of jsonSchema.metatoms) {
      console.log('Converting moleculoid', moleculeJson);

      molecules.push(Concore.Datacore.Molecule._inflate(jsonSchema.name, moleculeJson));
    }

    this._fireUploadQueue(molecules);
  }

  this.updateMoleculoid = function (schema) {
    console.log("🕐  Inflando moleculoid...");
    var moleculoidSchema = Moleculoid._inflateMoleculoid(schema);
    console.log("👍  Moleculoid inflado!");

    console.log("🕐  Carregando moleculoid...");
    return this.Moleculoid.get(schema.moleculoidId)
      .then(moleculoid => {
        console.log("👍  Moleculoid carregado!");
        const metatoms = moleculoid.getMetatoms();

        for (let metatom of metatoms) {
          console.log("❌  Removendo ", metatom.getLabel());
          moleculoid.removeMetatoms(metatom);
        }

        for (let metatom of moleculoidSchema.getMetatoms()) {
          console.log("✅  Adicionando ", metatom.name);
          moleculoid.addMetatoms(metatom);
        }

        return moleculoid.save();
      })
  }

  this._writeMoleculoid = function (moleculoid) {
    console.log("🕐  Salvando ", moleculoid.getLabel());

    // const moleculoidJson = {
    //   "name": moleculoid.getName(),
    //   "label": moleculoid.getLabel(),
    //   "icon": moleculoid.getIcon(),
    //   "metatoms": []
    // };

    // const metatomsToConvert = moleculoid.getMetatoms();
    // metatomsToConvert.forEach(metatomObj => {
    //   moleculoidJson.metatoms.push(metatomObj.attributes);
    // });

    const moleculoidJson = moleculoid.toJSON();

    fs.writeFileSync("./schema." + moleculoid.getName() + ".json", JSON.stringify(moleculoidJson, null, 2));
    console.log("👍  Moleculoid salvo em ./schema." + moleculoid.getName() + ".json");
  }

  this.downloadMoleculoid = function(id) {
    console.log("🕐  Carregando moleculoid " + id + " ...");
    return this.Moleculoid.get(id)
      .then(moleculoid => {
        console.log("👍  Moleculoid carregado!");
        this._writeMoleculoid(moleculoid);
      })
  }

  this.downloadAllMoleculoids = function () {
    console.log("🕐  Carregando moleculoids...");
    return this.Moleculoid.get()
      .then(moleculoids => {
        console.log("👍  Moleculoids carregados!");

        for (let moleculoid of moleculoids) {
          this._writeMoleculoid(moleculoid);
        }

        return moleculoids;
      })
  }

  this.downloadAllMolecules = function (moleculoidId) {
    console.log("🕐  Carregando moleculas de " + moleculoidId + "...");
    var query = new Concore.Datacore.MoleculeQuery(moleculoidId);

    return query.find()
      .then(resp => {
        if (resp.metadata.resultset.count === 0) {
          console.log("⚠️  Nenhum " + moleculoidId + " encontrado.")
          return;
        }

        console.log("👍  "+ resp.metadata.resultset.count + " " + moleculoidId + " carregados.");

        var molecules = resp.results;

        console.log("🕐  Salvando ", moleculoidId);
        fs.writeFileSync("./data." + moleculoidId + ".json", JSON.stringify(molecules, null, 2));
        console.log("👍  Moléculas salvas em ./data." + moleculoidId + ".json");
      })
  }

  this.destroyAllMolecules = function (moleculoidId) {
    console.log("🕐  Carregando moleculas de " + moleculoidId + "...");
    var query = new Concore.Datacore.MoleculeQuery(moleculoidId);
    query.limit(10000);

    return query.find()
      .then(resp => {
        if (resp.metadata.resultset.count === 0) {
          console.log("⚠️  Nenhum " + moleculoidId + " encontrado.")
          return;
        }

        console.log("👍  "+ resp.metadata.resultset.count + " " + moleculoidId + " carregados.");

        var molecules = resp.results;
        this._fireDestroyQueue(molecules);
      })
  }

  this.backup = function() {
    return this.downloadAllMoleculoids()
      .then(moleculoids => {
        this._fireBackupQueue(moleculoids);
      });
  }

  this.setPublicReadAccess = function(moleculoidId) {
    console.log("🕐  Alterando ACL do " + moleculoidId + "...");
    var query = new Concore.Datacore.MoleculeQuery(moleculoidId);

    return this.Moleculoid.get(moleculoidId)
      .then(moleculoid => {
        console.log("👍  Moleculoid carregado!");
        const acl = new Concore.Datacore.ACL();
        acl.setPublicReadAccess(true);
        moleculoid.setACL(acl);
        return moleculoid.save();
      });
  }

  this._fireBackupQueue = function (moleculoids) {
    console.log("\n👉  " + moleculoids.length + " Moleculoids restantes...");

    var moleculoid = moleculoids.shift();
    this.downloadAllMolecules(moleculoid.getId())
      .then(function() {
        if (moleculoids.length > 0) {
          this._fireBackupQueue(moleculoids);
        }
      });
  }

  this._fireUploadQueue = function (molecules) {
    console.log("\n👉  " + molecules.length + " Moléculas restantes...");

    var molecule = molecules.shift();
    console.log("🕐  Salvando molécula " + molecule.getTitle());

    // molecule.set('reembolsante', 'zS0ofIKQSB');

    molecule.save()
      .then((resp)=>{
        console.log("👍  " + resp.getTitle());
        if (molecules.length > 0) {
          this._fireUploadQueue(molecules);
        }
      })
      .catch(err => {
        console.log("😓  OPS!", err);
        if (molecules.length > 0) {
          this._fireUploadQueue(molecules);
        }
      });
  }

  this._fireDestroyQueue = function (molecules) {
    console.log("\n👉  " + molecules.length + " Moléculas restantes...");

    var molecule = molecules.shift();
    console.log("❌  Destruindo molécula " + molecule.getTitle());
    molecule.destroy()
      .then((resp)=>{
        console.log("👍  Feito!");
        if (molecules.length > 0) {
          this._fireDestroyQueue(molecules);
        }
      })
      .catch(err => {
        console.log("😓  OPS!", err);
        if (molecules.length > 0) {
          this._fireDestroyQueue(molecules);
        }
      });
  }

  this.deleteMoleculoid = function (moleculoidName) {
    return this.Moleculoid.get(moleculoidName)
      .then(moleculoid => {
        return moleculoid.destroy();
      });
  }

  return this;
}

module.exports = Genesis;