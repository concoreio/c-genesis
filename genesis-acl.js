#!/usr/bin/env node --harmony
var co = require('co');
var prompt = require('co-prompt');
var program = require('commander');
var config = require('./config.json');
var Genesis = require('./genesis.js');
var fs = require('fs');
var Concore = require('concore-sdk-js/lib/node/');

var loadJsonFile = function(path) {
  var fileAsString = fs.readFileSync(path).toString();
  var jsonFile = JSON.parse(fileAsString);
  return jsonFile;
}

var update = function(schemaUrl) {
  let genesis = Genesis(config);

  console.log("🕐  Autenticando usuário: " + config.username);
  genesis.auth(config.username, config.password)
    .then(resp => {
      console.log("👍  Usuário autenticado");

      console.log("🕐  Carregando schema...");
      var jsonFile = loadJsonFile(schemaUrl);
      console.log("👍  Schema carregado!");

      if (!jsonFile) {
        throw '😓  Erro ao tentar converter o json ou o arquivo esta vazio.';
        return;
      }

      console.log("🕐  Atualizando moleculoid...");
      return genesis.updateMoleculoid(jsonFile);
    })
    .then(resp => {
      console.log("👊  Feito!\n👉  ID: ", resp.getId());
    })
    .catch(err => {
      console.log('😓  OPS.. \n\n', err);
    })
}

program
  .version('1.0.0')
  .arguments('<file>')
  .action(update)
  .parse(process.argv);
